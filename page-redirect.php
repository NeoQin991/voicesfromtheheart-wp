<?php
/**
 * The template for top-level pages
 * Template name: Redirect
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fraynework
 */

 header( 'Location:'.get_field('link') ) ;
