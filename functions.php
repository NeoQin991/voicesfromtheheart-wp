<?php

/**
 * Just an entrance for modules
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package fraynework
 */

include_once 'inc/theme-tools.class.php';
$themeTools = new ThemeTools();
$themeTools->init();

include_once 'inc/theme-basics.class.php';
$themeBasics = new ThemeBasics();
$themeBasics->init();

include_once 'inc/theme-twists.class.php';
$themeTwists = new ThemeTwists();
$themeTwists->init();
