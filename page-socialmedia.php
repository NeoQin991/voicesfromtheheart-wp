<?php
/**
 * The template for displaying the homepage.
 * Template name: Social Media Page
 *
 * @package fraynework
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php get_template_part('template-parts/content', 'socialmedia'); ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
