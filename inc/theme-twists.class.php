<?php

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if (! class_exists('ThemeTwists')) :
    class ThemeTwists
    {
        public function __construct()
        {
        }

        public function init()
        {
            $this->loadModules();
        }

        public function loadModules()
        {
            include_once 'modules/option-page-settings.php';
            include_once 'modules/custom-post.php';
            include_once 'modules/custom-admin-screen.php';
            include_once 'modules/custom-gallery.php';
            include_once 'modules/google-analytics.php';
            include_once 'modules/extra-twists.php';
            include_once 'modules/custom-editor-table.php';
            include_once 'modules/api.php';
        }
    }

endif;
