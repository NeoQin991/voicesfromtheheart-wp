<?php
// Operating timestamp in different timezone
function _date($format="r", $timestamp=false, $timezone=false)
{
    $userTimezone = new DateTimeZone(!empty($timezone) ? $timezone : 'GMT');
    $gmtTimezone = new DateTimeZone('GMT');
    $myDateTime = new DateTime(($timestamp!=false?date("r", (int)$timestamp):date("r")), $gmtTimezone);
    $offset = $userTimezone->getOffset($myDateTime);
    return date($format, ($timestamp!=false?(int)$timestamp:$myDateTime->format('U')) + $offset);
}


// Get the id of the ancestor page given the post ID
function fw_get_ancestor_id(int $self_id)
{
    $parent_id = wp_get_post_parent_id($self_id);
    while (wp_get_post_parent_id($parent_id) != 0) {
        $parent_id = wp_get_post_parent_id($parent_id);
    }
    $ancestor_id = $parent_id;
    return $ancestor_id;
}
