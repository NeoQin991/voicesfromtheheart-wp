<?php
/*
 * It's a file to register extra minor twists
 */

/**
 *	Control Excerpt Length using Filters
 */
 function wpdocs_custom_excerpt_length( $length ) {
    return 20;
 }
 add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

 /**
 * Remove comment in admin menu
 * */
function remove_menus()
{
  remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'remove_menus');

 /**
  *	Control Excerpt Style
  */

 function new_excerpt_more( $more ) {
   	return ' ...';
 }
 add_filter('excerpt_more', 'new_excerpt_more');

 /**
  *	Custom Styles to WordPress Visual editor
  */

 function wpb_mce_buttons_2($buttons)
 {
     array_unshift($buttons, 'styleselect');
     return $buttons;
 }
 add_filter('mce_buttons_2', 'wpb_mce_buttons_2');


 /**
  * Callback function to filter the MCE settings
  */
 function my_mce_before_init_insert_formats($init_array)
 {

 // Define the style_formats array

     $style_formats = array(
 /*
 * Each array child is a format with it's own settings
 * Notice that each array has title, block, classes, and wrapper arguments
 * Title is the label which will be visible in Formats menu
 * Block defines whether it is a span, div, selector, or inline style
 * Classes allows you to define CSS classes
 * Wrapper whether or not to add a new block-level element around any selected elements
 */
         array(
             'title' => 'Leader Text',
             'block' => 'p',
             'classes' => 'leader',
             'wrapper' => false,
         )

     );
     // Insert the array, JSON ENCODED, into 'style_formats'
     $init_array['style_formats'] = json_encode($style_formats);
     return $init_array;
 }
 // Attach callback to 'tiny_mce_before_init'
 add_filter('tiny_mce_before_init', 'my_mce_before_init_insert_formats');



/**
 *  Allow svg upload
 */
 function add_svg_to_upload_mimes($upload_mimes)
 {
     $upload_mimes['svg'] = 'image/svg+xml';
     return $upload_mimes;
 }
 add_filter('upload_mimes', 'add_svg_to_upload_mimes', 10, 1);


/*
 * Login Screen Logo Link
 */
// changing the logo link from wordpress.org to your site
function mb_login_url() {  return home_url(); }
add_filter( 'login_headerurl', 'mb_login_url' );

// changing the alt text on the logo to show your site name
function mb_login_title() { return get_option( 'blogname' ); }
add_filter( 'login_headertitle', 'mb_login_title' );


/**
 * Add a new image format
 * @link https://en-au.wordpress.org/plugins/regenerate-thumbnails/
 */

add_image_size( 'top-banner',2400 , 1200);
add_image_size( 'supper',3600 , 1600);

// Example code to add image size to backend dropdown selector
// add_filter( 'image_size_names_choose', 'my_custom_sizes' );
// function my_custom_sizes( $sizes ) {
//     return array_merge( $sizes, array(
//         'size_slug' => __('Full name'),
//
//     ) );
// }
