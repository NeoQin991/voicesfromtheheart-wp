<?php
/*
 * Change WordPress default gallery output
 */

add_filter('post_gallery', 'ct__post_gallery', 10, 2);
function ct__post_gallery($output, $attr) {
global $post;

if (isset($attr['orderby'])) {
    $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
    if (!$attr['orderby'])
        unset($attr['orderby']);
}

extract(shortcode_atts(array(
    'order' => 'ASC',
    'orderby' => 'menu_order ID',
    'id' => $post->ID,
    'itemtag' => 'dl',
    'icontag' => 'dt',
    'captiontag' => 'dd',
    'columns' => 3,
    'size' => 'thumbnail',
    'include' => '',
    'exclude' => ''
), $attr));

$id = intval($id);
if ('RAND' == $order) $orderby = 'none';

if (!empty($include)) {
    $include = preg_replace('/[^0-9,]+/', '', $include);
    $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

    $attachments = array();
    foreach ($_attachments as $key => $val) {
        $attachments[$val->ID] = $_attachments[$key];
    }
}

if (empty($attachments)) return '';

// Here's your actual output, you may customize it to your need
$output = "<div class=\"slideshow-wrapper default\">\n";

// Now you loop through each attachment
foreach ($attachments as $id => $attachment) {
    // Fetch all data related to attachment
    $img = wp_prepare_attachment_for_js($id);

    // If you want a different size change 'large' to eg. 'medium'
    $url = $img['sizes']['large']['url']??$img['sizes']['full']['url'];
    //$alt = $img['alt'];
    // Store the caption
    $caption = $img['caption'];

    $output .= "<div class=\"slideshow-wrapper__item\">\n";
    $output .= "<div class=\"inner-slide-show-image\" style=\"background-image:url('{$url}')\"></div>\n";

    // Output the caption if it exists
    if ($caption) {
        $output .= "<div class=\"orbit-caption\">{$caption}</div>\n";
    }
    $output .= "</div>\n";
}

$output .= "</div>\n";

return $output;
}
