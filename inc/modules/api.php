<?php

/**
 * Twist Daily Prayer API
 * 
 * Route: /wp-json/acf/v3/posts/{id}
 */
add_filter( 'acf/rest_api/post/get_fields', function( $data, $request ) {
  if ( $request instanceof WP_REST_Request ) {
    $id = $request->get_param( 'id' );
  } else {
    $id = $request['id'];
  }
  $post = get_post( $id );
  $data['acf']['heading'] = isset( $post->post_title ) ? $post->post_title : '';
  return $data;
}, 10, 2);

/**
 * Support query Daily Prayer by date
 * 
 * Route: /wp-json/acf/v3/posts?prayer_date={date}
 */
add_filter( 'rest_post_query', function( $args, $request ) {
  $date = $request->get_param( 'prayer_date' );
  if ( ! empty( $date ) ) {
    $args['meta_query'] = [[
      'key'   => 'prayer_date',
      'value' =>  esc_sql( $date )
    ]];
  }
  return $args;
}, 10, 2);

/**
 * Twist Daily Prayer Search API
 * 
 * Route: /wp-json/acf/v3/posts?search={search}
 */
add_filter( 'rest_post_query', function( $args, $request ) {
  $search = $request->get_param( 'search' );
  if ( ! empty( $search ) ) {
    // modify the query arguments
    $args['meta_query'] = [[
      'key'     => 'prayer_date',
      'value'   =>  date( 'Ymd' ),
      'compare' =>  '<='
    ]];
  }
  return $args;
}, 10, 2);

/**
 * Get Weekly Background Image
 */
add_action( 'rest_api_init', function() {
  register_rest_route( 'acf/v3', '/background', array(
    'methods' => 'GET',
    'callback' => function($request) {
      $background = get_posts([
        "posts_per_page"   => 1,
        "post_type"        => "image",
        "post_status"      => "publish"
      ])[0];
      $size = $request->get_param( 'size' );
      $image_id = get_field('images', $background->ID);
      if ($image_id) {
        return wp_get_attachment_image_url($image_id, empty($size) ? 'medium_large' : trim($size));
      } else {
        return '';
      }
    }
  ) );
} );

/**
 * Twist Link API
 * 
 * Route: /wp-json/acf/v3/link
 */
add_filter( 'acf/rest_api/link/get_items', function( $response ) {
  $_response = array_map(function( $item ) {
    $item['acf']['title'] = get_the_title( $item['id'] );
    return $item;
  }, $response->get_data());
  return rest_ensure_response( $_response );
}, 10, 1);


