<?php

/**
 * Hide default Tax and Comments
 */
function fw_sub_menu_page_removing() {
  remove_menu_page('edit-comments.php'); // comments
  remove_submenu_page( 'edit.php','edit-tags.php?taxonomy=post_tag' );

}
add_action( 'admin_menu', 'fw_sub_menu_page_removing' );


function fw_post_edit_columns($columns){
  unset($columns['tags']);
  unset($columns['comments']);
  return $columns;
}
add_filter("manage_edit-post_columns", "fw_post_edit_columns");

/*
 * Add columns to daily prayer list
 */
function add_acf_columns( $columns ) {
  $modified_columns = [];
  foreach ( $columns as $k => $value ) {
    if ( $k == 'author' ) {
      $modified_columns['prayer_date'] = __( 'Prayer Date' );
    }
    $modified_columns[$k] = $value;
  }
  return $modified_columns;
}
add_filter( 'manage_post_posts_columns', 'add_acf_columns' );

/*
 * Add columns to exhibition post list
 */
function post_custom_column( $column, $post_id ) {
  if ( $column == 'prayer_date' ) {
    echo acf_format_date(
      get_field( 'prayer_date', $post_id ),
      get_field_object( 'prayer_date' )[ 'display_format' ]
    );
  }
}
add_action( 'manage_post_posts_custom_column', 'post_custom_column', 10, 2 );