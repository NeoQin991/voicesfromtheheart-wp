<?php
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme settings',
		'menu_title'	=> 'Theme settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));
	//
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'General Settings',
	// 	'menu_title'	=> 'General Settings',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Social & Contacts',
		'menu_title'	=> 'Social & Contacts',
		'parent_slug'	=> 'theme-general-settings',
	));


	acf_add_options_sub_page(array(
		'page_title' 	=> 'Banner Settings',
		'menu_title'	=> 'Banner Settings',
		'parent_slug'	=> 'theme-general-settings',
	));
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Footer Settings',
	// 	'menu_title'	=> 'Footer Settings',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
}
