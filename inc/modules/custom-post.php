<?php

/**
 * Example code to register a custom post type.
 * @link https://codex.wordpress.org/Function_Reference/register_post_type
 */

// add_action( 'init', 'codex_book_init' );
// function codex_book_init() {
// 	$labels = array(
// 		'name'               => _x( 'Books', 'post type general name', 'fraynework' ),
// 		'singular_name'      => _x( 'Book', 'post type singular name', 'fraynework' ),
// 		'menu_name'          => _x( 'Books', 'admin menu', 'fraynework' ),
// 		'name_admin_bar'     => _x( 'Book', 'add new on admin bar', 'fraynework' ),
// 		'add_new'            => _x( 'Add New', 'book', 'fraynework' ),
// 		'add_new_item'       => __( 'Add New Book', 'fraynework' ),
// 		'new_item'           => __( 'New Book', 'fraynework' ),
// 		'edit_item'          => __( 'Edit Book', 'fraynework' ),
// 		'view_item'          => __( 'View Book', 'fraynework' ),
// 		'all_items'          => __( 'All Books', 'fraynework' ),
// 		'search_items'       => __( 'Search Books', 'fraynework' ),
// 		'parent_item_colon'  => __( 'Parent Books:', 'fraynework' ),
// 		'not_found'          => __( 'No books found.', 'fraynework' ),
// 		'not_found_in_trash' => __( 'No books found in Trash.', 'fraynework' )
// 	);
//
// 	$args = array(
// 		'labels'             => $labels,
// 		'description'        => __( 'Description.', 'fraynework' ),
// 		'public'             => true,
// 		'publicly_queryable' => true,
// 		'show_ui'            => true,
// 		'show_in_menu'       => true,
// 		'query_var'          => true,
// 		'rewrite'            => array( 'slug' => 'book' ),
// 		'capability_type'    => 'post',
// 		'has_archive'        => true,
// 		'hierarchical'       => false,
// 		'menu_position'      => null,
// 		'exclude_from_search'=> false,
//    'menu_icon'          => 'dashicons-media-text',
// 		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
// 	);
//
// 	register_post_type( 'book', $args );
// }


/**
 * Example code to register a custom taxonomy.
 * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
 */
// add_action( 'init', 'create_video_tax',0 );
// function create_video_tax() {
//   // Add new taxonomy, make it hierarchical (like categories)
//   $labels = array(
//     'name'              => _x( 'Video Category', 'taxonomy general name', 'textdomain' ),
//     'singular_name'     => _x( 'Video Category', 'taxonomy singular name', 'textdomain' ),
//     'search_items'      => __( 'Search Categories', 'textdomain' ),
//     'all_items'         => __( 'All Categories', 'textdomain' ),
//     'edit_item'         => __( 'Edit Category', 'textdomain' ),
//     'update_item'       => __( 'Update Category', 'textdomain' ),
//     'add_new_item'      => __( 'Add New Category', 'textdomain' ),
//     'new_item_name'     => __( 'New Category Name', 'textdomain' ),
//     'menu_name'         => __( 'Video Category', 'textdomain' ),
//   );
//   $args = array(
//     'hierarchical'      => true,
//     'labels'            => $labels,
//     'show_ui'           => true,
//     'show_admin_column' => true,
//     'query_var'         => true,
//     'rewrite'           => array( 'slug' => 'video_category' ),
//   );
//   register_taxonomy( 'video_category', array( 'video' ), $args );
// }

// Chnage "Post" to "Prayer"
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Daily Prayer';
    $submenu['edit.php'][5][0] = 'Prayer';
    $submenu['edit.php'][10][0] = 'Add New Prayer';
    $submenu['edit.php'][16][0] = 'Prayer Tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Prayer';
    $labels->singular_name = 'Prayer';
    $labels->add_new = 'Add Prayer';
    $labels->add_new_item = 'Add Prayer';
    $labels->edit_item = 'Edit Prayer';
    $labels->new_item = 'Prayer';
    $labels->view_item = 'View Prayer';
    $labels->search_items = 'Search Prayer';
    $labels->not_found = 'No Prayer found';
    $labels->not_found_in_trash = 'No Prayer found in Trash';
    $labels->all_items = 'All Prayers';
    $labels->menu_name = 'Prayers';
    $labels->name_admin_bar = 'Daily Prayer';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );


function cptui_register_my_cpts_new_link() {

	/**
	 * Post Type: Link.
	 */

	$labels = array(
		"name" => __( 'Links', '' ),
		"singular_name" => __( 'Link', '' ),
	);
	$labels = array(
		'name'               => _x( 'Links', 'post type general name', 'origin' ),
		'singular_name'      => _x( 'Link', 'post type singular name', 'origin' ),
		'menu_name'          => _x( 'Links', 'admin menu', 'origin' ),
		'name_admin_bar'     => _x( 'Link', 'add new on admin bar', 'origin' ),
		'add_new'            => _x( 'Add New', 'Link', 'origin' ),
		'add_new_item'       => __( 'Add New Link', 'origin' ),
		'new_item'           => __( 'New Link', 'origin' ),
		'edit_item'          => __( 'Edit Link', 'origin' ),
		'view_item'          => __( 'View Link', 'origin' ),
		'all_items'          => __( 'All Links', 'origin' ),
		'search_items'       => __( 'Search Links', 'origin' ),
		'parent_item_colon'  => __( 'Parent Links:', 'origin' ),
		'not_found'          => __( 'No Links found.', 'origin' ),
		'not_found_in_trash' => __( 'No Links found in Trash.', 'origin' )
	);

	$args = array(
		"label" => __( 'Links', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
    "menu_icon" => "dashicons-share",
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "link", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title"),
	);

	register_post_type( "link", $args );
}

add_action( 'init', 'cptui_register_my_cpts_new_link' );



function cptui_register_my_cpts_new_img() {

	/**
	 * Post Type: Events.
	 */

	$labels = array(
		"name" => __( 'Images', '' ),
		"singular_name" => __( 'Image', '' ),
	);
	$labels = array(
		'name'               => _x( 'Images', 'post type general name', 'origin' ),
		'singular_name'      => _x( 'Image', 'post type singular name', 'origin' ),
		'menu_name'          => _x( 'Homepage Background', 'admin menu', 'origin' ),
		'name_admin_bar'     => _x( 'Image', 'add new on admin bar', 'origin' ),
		'add_new'            => _x( 'Add New', 'img', 'origin' ),
		'add_new_item'       => __( 'Add New Image', 'origin' ),
		'new_item'           => __( 'New Image', 'origin' ),
		'edit_item'          => __( 'Edit Image', 'origin' ),
		'view_item'          => __( 'View Image', 'origin' ),
		'all_items'          => __( 'All Images', 'origin' ),
		'search_items'       => __( 'Search Images', 'origin' ),
		'parent_item_colon'  => __( 'Parent Images:', 'origin' ),
		'not_found'          => __( 'No images found.', 'origin' ),
		'not_found_in_trash' => __( 'No images found in Trash.', 'origin' )
	);

	$args = array(
		"label" => __( 'Images', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
    "menu_icon" => "dashicons-calendar-alt",
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "image", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "image", $args );
}

  add_action( 'init', 'cptui_register_my_cpts_new_img' );
