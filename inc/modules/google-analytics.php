<?php
function fw_google_analytics_tracking_code()
{
  if ($google_analytics_id = get_field('google_analytics_id', 'option')): ?>
		 <script>
		   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		   })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		   ga('create', '<?php echo $google_analytics_id ?>', 'auto');
		   ga('send', 'pageview');
		 </script>
<?php
  endif;
}
// include GA tracking code before the closing head tag
add_action('wp_head', 'fw_google_analytics_tracking_code', 10 );
