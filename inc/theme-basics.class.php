<?php

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if (! class_exists('ThemeBasics')) :
    class ThemeBasics
    {
        private $version;

        public function __construct($version = '1.0.0')
        {
          $this->version = $version;
        }

        public function init()
        {
            $this->hooks();
        }
        private function hooks()
        {
            add_action('after_setup_theme', [ $this, 'fwSetup' ]);
            add_action('wp_enqueue_scripts', [$this , 'fwScripts'], 20);
            add_action('login_enqueue_scripts', [$this , 'fwLoginStylesheet'], 20);
            add_action('init', [$this , 'fwEditorStyles'], 20);
            add_action('admin_enqueue_scripts', [$this , 'fwEditorScript'], 20);
        }

        public function fwSetup()
        {
            /*
             * Make theme available for translation.
             * Translations can be filed in the /languages/ directory.
             * If you're building a theme based on fraynework, use a find and replace
             * to change 'fraynework' to the name of your theme in all the template files.
             */
            load_theme_textdomain('fraynework', get_template_directory() . '/languages');

            /*
             * Let WordPress manage the document title.
             * By adding theme support, we declare that this theme does not use a
             * hard-coded <title> tag in the document head, and expect WordPress to
             * provide it for us.
             */
            add_theme_support('title-tag');

            /*
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support('html5', array(
                'search-form',
                'gallery',
                'caption',
            ));

            /*
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support('post-thumbnails');

            // This theme uses wp_nav_menu() in one location.
            register_nav_menus(array(
                'Primary' => esc_html__('Primary', 'fraynework'),
            ));

            register_nav_menus(array(
                'Footer' => esc_html__('Footer', 'fraynework'),
            ));

            register_nav_menus(array(
                'Sub Footer' => esc_html__('Sub Footer', 'fraynework'),
            ));
        }

        public function fwScripts()
        {
            wp_enqueue_style('fraynework-style', get_template_directory_uri() . '/front-end/assets/css/style.css', array(), $this->version, 'all');
            wp_enqueue_script('fraynework-main', get_template_directory_uri() . '/front-end/assets/js/main.js', array('jquery'), $this->version, true);
            /*
             * Conditionally adding js file, below is an example for Home page.
             */
            // if(is_front_page()){
            //   wp_enqueue_script('fraynework-extra', get_template_directory_uri() . '/front-end/assets/js/extra.js', array('jquery'), $this->version, false);
            // }
        }

        public function fwLoginStylesheet()
        {
            wp_enqueue_style('custom-login', get_stylesheet_directory_uri() . '/front-end/assets/css/login.css');
        }

        public function fwEditorStyles()
        {
            add_editor_style('/front-end/assets/css/editor.css'); //slash before "assets" should be removed.
        }
        public function fwEditorScript()
        {
            wp_enqueue_script('fraynework-admin', get_template_directory_uri() . '/front-end/assets/js/admin.js', array(), $this->version, false);
        }
    }


endif;

    add_action('homepage_auto_change','homepage_auto_change_function');
    function homepage_auto_change_function(){
      $args = array(
      'posts_per_page'=>-1,
      'post_type'=>'image',
      );
      $posts_array = get_posts( $args );


        global $wpdb;
        // $wpdb->query( 'SELECT @i:=-1' );
        $wpdb->query(
            "
            UPDATE wp_posts SET menu_order = menu_order - 1 WHERE post_type = 'image' ;
            "
        );
        $wpdb->query(
            "
            UPDATE wp_posts SET menu_order = ". sizeof($posts_array) ." WHERE post_type = 'image' and menu_order = 0;
            "
        );
    }

    add_action('homepage_add_year','homepage_auto_add_year');
    function homepage_auto_add_year(){
      $args = array(
      'posts_per_page'=>-1,
      'post_type'=>'post',
      );
      $posts_array = get_posts( $args );
      foreach ( $posts_array as $post ) {
         $temp_date = get_post_meta($post->ID, 'prayer_date', true);
         update_post_meta( $post->ID,  'prayer_date', (string)((int)$temp_date + 10000) );
      }
    }

    function set_new_browser_user_cookie() {
    // echo $test2;
      if ( !isset( $_COOKIE['not_new_browser_user'])) {
          setcookie( 'not_new_browser_user', 1, time() + 3600, COOKIEPATH, COOKIE_DOMAIN, false );
      }
    }
    add_action( 'init', 'set_new_browser_user_cookie', 10 , 2 );


    // function remove_posts_from_wp_search($query) {
    // if ($query->is_search) {
    // $query->set('post_type', 'page');
    // }
    // return $query;
    // }
    // add_filter('pre_get_posts','remove_posts_from_wp_search');
