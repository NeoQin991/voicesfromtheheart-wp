<?php

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if (! class_exists('ThemeTools')) :
    class ThemeTools
    {
        public function __construct()
        {
        }

        public function init()
        {
            $this->loadModules();
        }

        public function loadModules()
        {
        // Only load functions here, do not load code with "add_action" or "add_filter"
            include_once 'modules/minor-functions.php';
        }
    }

endif;

add_filter('body_class', 'custom_body_class');
function custom_body_class($classes) {
      if (isset($_GET['from'])&&$_GET['from'] == 'app'):
        $classes[] = 'from-app';
      endif;
    return $classes;
}
