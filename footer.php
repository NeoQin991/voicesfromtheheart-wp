<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fraynework
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php wp_nav_menu( array( 'theme_location' => 'Footer', 'menu_class' => 'footer-menu') ); ?>
		<div class="sub-footer-wrapper">
			<div class="copyright">Copyright © <?php echo date('Y') ?> Fraynework Multimedia</div>
			<a href="https://www.fraynework.com.au" target="_blank" class="f-logo"><img src="<?php echo get_template_directory_uri()?>/front-end/assets/img/fraynework.svg" alt="Fraynework Multimedia" class="f-logo-image"></a>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
