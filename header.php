<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fraynework
 */

?>
<!DOCTYPE html>
<html lang="en-AU">
  <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
  <div id="page" class="site">
  	<header id="masthead" class="site-header" role="banner">
      <div class="header-inner">
        <div class="site-branding">
          <div class="logo-inner">
            <?php
            if ( is_front_page() ) :?>
              <h1 class="site-title"><a href="<?php echo esc_url( home_url() ); ?>" rel="home"><img src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/home-logo.svg"  alt="<?php echo get_bloginfo();?>" ></a></h1>
            <?php else : ?>
              <p class="site-title"><a href="<?php echo esc_url( home_url() ); ?>" rel="home"><img src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/home-logo.svg" alt="<?php echo get_bloginfo();?>" ></a></p>
            <?php
          endif; ?>
          </div>


        </div>
        <nav id="site-navigation" class="main-navigation" role="navigation">
        <div class="main-wrapper">
          <?php wp_nav_menu( array( 'theme_location' => 'Primary', 'menu_class' => 'primary-menu') ); ?>
        </div>



        </nav><!-- #site-navigation -->
        <button class="menu-toggle hamburger hamburger--squeeze" aria-expanded="false">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
        <div class="gear-icon">
          <img class="gear" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/gear.png" alt="">
          <img class="cross" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/cross.svg" alt="">
        </div>

        <div class="header-timer">
          <div class="time-wrapper">
            <div class="header-time-lead">
              Timer
            </div>
            <div class="header-time">
             00:00
            </div>
          </div>

          <div class="time-timer-outer">
            <div class="timer-time">
              <div class="progressBar">
                <div class="bar-outer">
                  <div class="bar-inner">

                  </div>
                </div>
                <div class="circle">

                </div>
              </div>
            </div>
          </div>

          <div class="header-time-updated">
            UPDATE
          </div>
        </div>

      </div>



  	</header><!-- #masthead -->
  	<div id="content" class="site-content">
