<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fraynework
 */

?>

<div class="main-wrapper innerpages">
	<?php get_template_part( 'template-parts/module', 'top-banner' );	?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<?php if(!is_page()): ?>
					<div class="post-date">
						<?php echo get_the_date(); ?>
					</div>
				<?php endif; ?>
				<div class="default-content-wrapper">
					<div class="app-page-title">
						<?php the_title(); ?>
					</div>
					<div class="editor-content">
						<?php the_content(); ?>
					</div>
						<?php get_template_part('template-parts/module', 'builder'); ?>
				</div>

			</div><!-- .entry-content -->
			<?php if ( get_edit_post_link() ) : ?>
				<footer class="entry-footer">
					<?php
						edit_post_link(
							sprintf(
								/* translators: %s: Name of current post */
								esc_html__( 'Edit %s', 'fraynework' ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							),
							'<span class="edit-link">',
							'</span>'
						);
					?>
				</footer><!-- .entry-footer -->
			<?php endif; ?>
		</article><!-- #post-## -->
</div>
