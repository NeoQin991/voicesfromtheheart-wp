<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fraynework
 */

?>

<div class="main-wrapper innerpages">
	<?php get_template_part( 'template-parts/module', 'top-banner' );	?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<?php if(!is_page()): ?>
					<div class="post-date">
						<?php echo get_the_date(); ?>
					</div>
				<?php endif; ?>
				<div class="default-content-wrapper">
					<div class="app-page-title">
						<?php the_title(); ?>
					</div>
					<div class="editor-content">
						<?php wp_reset_query(); ?>
						<?php the_content(); ?>
					</div>
					<div class="contact-block">
						<div class="left">
							<div class="title">
								Street/Postal address
							</div>
							<div class="address">
								<?php echo get_field('address','option') ?>
							</div>
							<?php if ( get_field('phone','option') ) : ?>
								<div class="tel">
									<span class="leader">T</span>
									<a href="tel:<?php echo get_field('phone','option') ?>">
											<span class="tel-no"><?php echo get_field('phone','option') ?></span>
									</a>
								</div>
							<?php endif; ?>
							<?php if ( get_field('fax','option') ) : ?>
								<div class="fax">
									<span class="leader">F</span>
									<span class="fax-no"><?php echo get_field('fax','option') ?></span>
								</div>
							<?php endif; ?>
							<?php if ( get_field('email','option') ) : ?>
								<a href="mailto:<?php echo get_field('email','option') ?>">
									<div class="email">
										<span class="leader">E</span>
										<span class="email-add"><?php echo get_field('email','option') ?></span>
									</div>
								</a>
							<?php endif; ?>
						</div>
						<div class="right">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3311.963914805028!2d151.24347945171755!3d-33.89058318055535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12adf07c670e95%3A0x2b0244107d1b8c02!2s2%2F55+Grafton+St%2C+Bondi+Junction+NSW+2022!5e0!3m2!1sen!2sau!4v1553042171341" width="100%" height="224" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
						<?php get_template_part('template-parts/module', 'builder'); ?>
				</div>

			</div><!-- .entry-content -->
			<?php if ( get_edit_post_link() ) : ?>
				<footer class="entry-footer">
					<?php
						edit_post_link(
							sprintf(
								/* translators: %s: Name of current post */
								esc_html__( 'Edit %s', 'fraynework' ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							),
							'<span class="edit-link">',
							'</span>'
						);
					?>
				</footer><!-- .entry-footer -->
			<?php endif; ?>
		</article><!-- #post-## -->
</div>
