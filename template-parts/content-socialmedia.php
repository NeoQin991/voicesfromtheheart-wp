<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fraynework
 */

?>

<div class="main-wrapper innerpages">
	<?php get_template_part( 'template-parts/module', 'top-banner' );	?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<?php if(!is_page()): ?>
					<div class="post-date">
						<?php echo get_the_date(); ?>
					</div>
				<?php endif; ?>
				<div class="default-content-wrapper">
					<div class="app-page-title">
						<?php the_title(); ?>
					</div>
					<div class="editor-content">
						<?php wp_reset_query(); ?>
						<?php the_content(); ?>
					</div>

					<div class="socialmedia-block">
						<div class="social-inner">
							<div class="facebook">
								<div class="btn-area">
									<img class="svg" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/facebook.svg" alt="">
									<span>Facebook</span>
								</div>
								<img class="down-arrow" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/downarrow.svg" alt="">
								<div class="pages">
									<?php
											wp_reset_query();
										if (have_rows('facebook','option')): // loop through the rows of data
											while (have_rows('facebook','option')) :
												the_row(); ?>
													<a href="<?php echo get_sub_field('facebook_page_url','option') ?>" target="_blank">
														<div class="pagename">
															<?php echo get_sub_field('facebook_page_name','option') ?>
														</div>
													</a>
									<?php

									endwhile;
										endif;
									 	?>
								</div>
							</div>

							<div class="ins">
								<div class="btn-area">
									<img class="svg" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/ins.svg" alt="">
									<span>Instagram</span>
								</div>
								<img class="down-arrow" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/downarrow.svg" alt="">
								<div class="pages">
									<?php
											wp_reset_query();
										if (have_rows('instagram','option')): // loop through the rows of data
											while (have_rows('instagram','option')) :
												the_row(); ?>
													<a href="<?php echo get_sub_field('instagram_page_url','option') ?>" target="_blank">
														<div class="pagename">
															<?php echo get_sub_field('instagram_page_name','option') ?>
														</div>
													</a>
									<?php

									endwhile;
										endif;
									 	?>
								</div>
							</div>

							<div class="twitter">
								<div class="btn-area">
									<img class="svg" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/twitter.svg" alt="">
									<span>Twitter</span>
								</div>
								<img class="down-arrow" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/downarrow.svg" alt="">
								<div class="pages">
									<?php
											wp_reset_query();
										if (have_rows('twitter','option')): // loop through the rows of data
											while (have_rows('twitter','option')) :
												the_row(); ?>
													<a href="<?php echo get_sub_field('twitter_page_url','option') ?>" target="_blank">
														<div class="pagename">
															<?php echo get_sub_field('twitter_page_name','option') ?>
														</div>
													</a>
									<?php

									endwhile;
										endif;
									 	?>
								</div>
							</div>

						</div>
					</div>


						<?php get_template_part('template-parts/module', 'builder'); ?>
				</div>

			</div><!-- .entry-content -->
			<?php if ( get_edit_post_link() ) : ?>
				<footer class="entry-footer">
					<?php
						edit_post_link(
							sprintf(
								/* translators: %s: Name of current post */
								esc_html__( 'Edit %s', 'fraynework' ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							),
							'<span class="edit-link">',
							'</span>'
						);
					?>
				</footer><!-- .entry-footer -->
			<?php endif; ?>
		</article><!-- #post-## -->
</div>
