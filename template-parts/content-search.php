<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fraynework
 */

?>

<div class="result-item">
	<header class="entry-header">
		<h2 class="entry-title">	<?php the_title( )?></h2>
	</header><!-- .entry-header -->
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<p>
		<a target="_blank" href="<?php echo get_permalink() ?>"> <?php echo get_permalink() ?> </a>
	</p>
</div>
