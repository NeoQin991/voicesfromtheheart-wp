<?php
if( have_rows('content_builder') ):
  $row = 0;
// loop through the rows of data
    while ( have_rows('content_builder') ) : the_row(); $row++;
        // Default Content
        if( get_row_layout() == 'default_content' ):?>
          <div id="row-<?php echo $row?>" class="editor-content"><?php the_sub_field('content');  ?></div>
      <?php
        endif;
        // Innerpage Gallery Flexible Size
        if( get_row_layout() == 'gallery_carousel_img' ): ?>
        <div id="row-<?php echo $row?>">
          <div class="slideshow-wrapper with-arrow flexible">
          <?php foreach(get_sub_field("images") as $image): ?>
            <div class="img-container">
              <img src="<?php echo $image['sizes']['large']?>" alt="slide">
              <?php if($image['caption']!=''): ?>
                <div class="orbit-caption">
                  <?php echo $image['caption'] ?>
                </div>
              <?php endif ?>
            </div>
          <?php endforeach; ?>
          </div>
          <script type="text/javascript">
          jQuery(document).ready(function($) {
            $('#row-<?php echo $row?> .slideshow-wrapper').slick({
              dots: true,
              autoplay:<?php echo get_sub_field('autoplay');?>,
              autoplaySpeed:<?php echo get_sub_field('autoplay_speed')*1000;?>,
              arrows:false,
              infinite: true,
              speed: 500,
              fade:true,
              //adaptiveHeight: true,
            });
          });
          </script>
        </div>
        <?php
      endif;

      if( get_row_layout() == 'download_box' ):
        $the_link = get_sub_field('external')?get_sub_field('link'):get_sub_field('file');
        $the_text = get_sub_field('external')?'Visit':'Download';
        ?>
        <a href="<?php echo $the_link ?>" target="_blank" id="row-<?php echo $row?>" class="download_box">
          <div class="inner-wrapper">
            <div class="title"><?php the_sub_field('title') ?></div>
            <div class="icon-wrapper"><span><?php echo $the_text ?> </span><img class="download-icon" alt="icon" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/download.svg"></div>
          </div>
        </a>
        <?php
      endif;

    endwhile;
  endif;
