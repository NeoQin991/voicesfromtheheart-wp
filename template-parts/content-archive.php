<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fraynework
 */

?>

<div class="main-wrapper innerpages">
	<?php get_template_part( 'template-parts/module', 'top-banner' );	?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<?php if(!is_page()): ?>
					<div class="post-date">
						<?php echo get_the_date(); ?>
					</div>
				<?php endif; ?>
				<div class="default-content-wrapper">
					<div class="app-page-title">
						<?php the_title(); ?>
					</div>
					<div class="editor-content">
						<?php wp_reset_query(); ?>
						<?php the_content(); ?>
					</div>

					<div class="archive-block">
						<div class="search-block">
							<div class="search-wrapper">

								<input type="text" name="" placeholder="Search Achives" value="">
								<img class="search-icon" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/search-icon.svg" alt="">
							</div>

							<div class="result-list">
								<div class="return-item" value="20190321">

								</div>
							</div>
						</div>
						<div class="left-calendar">
							<div class="full-calendar">
										<script>
										(function($) {
											var highlight_these_events = [];
											function highlightSelectedEvents(date) {
													for (var index = 0; index < highlight_these_events.length; index++) {
															if($(highlight_these_events[index]).html() == date){
																$(highlight_these_events[index]).addClass('active');
																// alert(date);
															}
													}
											}

											function disableEventAfter(date) {
													for (var index = 0; index < highlight_these_events.length; index++) {
															if($(highlight_these_events[index]).html() >= date){
																$(highlight_these_events[index]).addClass('today-later');
																// alert(date);
															}
													}
											}
											var today = $(this).attr('value');
								      var d = new Date();
								      var month = d.getMonth()+1;
								      var day = d.getDate();
								      var output_date = d.getFullYear() + (month<10 ? '0' : '') + month  + (day<10 ? '0' : '') + day;
								      // get_home_prayer_feed();
								      // get_archive_prayer_feed(output_date);
											window.mobilecheck = function() {
												var check = false;
												(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
												return check;
											};
											$(document).ready(function() {
												$('.google-calendar').fullCalendar({
													height: 502,

													header: {
														left: 'prev,next',
														center: 'title',
														right: 'agendaWeek,month,listMonth'
													},
													// timeFormat: 'hh:mm a',
													// defaultView: window.mobilecheck() ? "listMonth" : "month",
													displayEventTime: false, // don't show the time column in list view
													// THIS KEY WON'T WORK IN PRODUCTION!!!
													// To make your own Google API key, follow the directions here:
													// http://fullcalendar.io/docs/google_calendar/
													timezone:'Australia/Sydney',
													displayEventEnd:true,
													events: '<?php echo get_template_directory_uri()?>/template-parts/module-prayers-feed.php',
													eventRender: function(event,element){
														  // element.find('.fc-list-item-title,.fc-title').html(event.title);
														},
													// eventAfterAllRender: function(){
													//
													// },
													eventClick: function(event) {
														// opens events in a popup window
														// window.location.href = event.url;
														return false;
													},
													eventAfterAllRender: function(){
														highlight_these_events = $(".entry-content .fc-day-grid-event .fc-content span").toArray();
														// alert(highlight_these_events.length);
														highlightSelectedEvents(output_date);
														disableEventAfter(output_date);
												    }
												});
											});
										})( jQuery );

									</script>
								<div class='google-calendar'>
								</div>
							</div>
						</div>



						<div class="right-prayer-block">
							<div class="prayer-date">

							</div>
							<div class="gospel-title">

							</div>
							<div class="gospel-content">
							</div>
							<div class="gospel-quote">

							</div>
							<div class="reflective-que">

							</div>

							<div class="closing-part">
								<div class="closing-content">

								</div>
								<div class="closing-source">

								</div>
							</div>
							<div class="no-prayer">
								Sorry, No Prayer Today!
							</div>
						</div>
					</div>
						<?php get_template_part('template-parts/module', 'builder'); ?>
				</div>
				<div class="search-cover">

				</div>
			</div><!-- .entry-content -->
			<?php if ( get_edit_post_link() ) : ?>
				<footer class="entry-footer">
					<?php
						edit_post_link(
							sprintf(
								/* translators: %s: Name of current post */
								esc_html__( 'Edit %s', 'fraynework' ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							),
							'<span class="edit-link">',
							'</span>'
						);
					?>
				</footer><!-- .entry-footer -->
			<?php endif; ?>
		</article><!-- #post-## -->
</div>
