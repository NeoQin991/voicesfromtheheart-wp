<?php
$self_id = get_the_ID();

if (is_page()):
    //For the banner title (usually it's the ancestor page id)
    $ancestor_id = fw_get_ancestor_id($self_id);
    if ($ancestor_id!= 0):
        $section_title = get_the_title($ancestor_id);
    else:
        $section_title = get_the_title(); // the page is at top level.
    endif;

    //For the banner image(usually it's the section image)
    if (has_post_thumbnail()):
        $section_image = get_the_post_thumbnail_url($self_id, 'top-banner');
    elseif (has_post_thumbnail($ancestor_id)):
        $section_image = get_the_post_thumbnail_url($ancestor_id, 'top-banner');
    else:
        $section_image = get_field('header_image', 'option')['sizes']['top-banner'];
    endif;
else:
    // For the title
    if (is_search()):
        $section_title = "Search Results";
        $section_image = get_field('header_image', 'option')['sizes']['top-banner'];
        $ancestor_id = 0;

    elseif (is_404()):
        $section_title = "Not Found";
        $section_image = get_field('header_image', 'option')['sizes']['top-banner'];
        $ancestor_id = 0;

    else: // for post
        $news_centre = get_field('news_centre', 'option');
        $section_title = get_the_title($news_centre);
        $ancestor_id = $news_centre;

        if (get_field('using_feature')):
          //user wants to use the featured image as section image
            $section_image = get_the_post_thumbnail_url($self_id, 'top-banner')??'';
        elseif($news_centre):
          //user wants to use the default section image, which is the landing page's featured image.
            $section_image = get_the_post_thumbnail_url($news_centre, 'top-banner')??'';
        else:
          //when the landing page hasn't been selected
            $section_image = '';
        endif;
    endif;
endif;
?>
<div class='top-banner' style="background-image:url('<?php echo $section_image; ?>')">
  <div class="page-title">
    <?php the_title(); ?>
  </div>
</div>
