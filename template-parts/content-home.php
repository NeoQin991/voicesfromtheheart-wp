<?php
/**
 * Template part for displaying page content in page-homepage.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fraynework
 */
?>

<?php
  $new_user = '';
  if(isset($_COOKIE['not_new_browser_user'])){
    $new_user = '';
  }
  else{
    $new_user = 'new-user';
  }
 ?>
<div class="start-home <?php echo $new_user; ?>" style="background-image:url('<?php echo get_template_directory_uri() ?>/front-end/assets/img/home-start.png')">
  <div class="new-user-inner">
    <div class="content-area">
      <img src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/starter-logo.svg" alt="">
      <div class="leader-text">
        We invite you to ponder God’s presence around our table and share in ‘voices from the heart.’
      </div>
      <div class="start-btn">
        Click here to begin
      </div>
    </div>
  </div>
</div>


<div class="main-wrapper home-wrapper">
  <?php

  wp_reset_query();
    $home_page_background = '';
    $home_query=new WP_Query(array(
           'post_type' => 'image',
           'posts_per_page' => 1,
        ));
        if ( $home_query->have_posts() ) :
           $home_query->the_post();
           // var_dump(the_post());
            ?>
              <div class="home-background <?php echo $post->menu_order?>" style="background-image:url('<?php echo wp_get_attachment_image_url(get_field('images'), 'full')?>')">
       <?php
        endif;

   ?>


    <div class="top__image-mask">
    </div>
    <div class="bottom__image-mask">
    </div>
  </div>
  <div class="home-content-wrapper">
    <div class="home-content-inner">
      <div class="calendar-part">
        <div class="calendar-area">
          <img src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/calendar.png" alt="">
          <div class="c-title">
            CALENDAR
          </div>
          <img class="down-arrow" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/down-arrow.svg" alt="">
        </div>
        <div class="calendar-items">
          <div class="clendar-item-wrapper">
            <?php
            wp_reset_query();
            $month = date('m');
            // $test = date('M',$month);
             $monthName = date('M', mktime(0, 0, 0, $month, 10));
             $month_start = date("y").'-'.$month.-'01';
             $start = date('Ym01', strtotime($month_start));
             $end = date('Ymt', strtotime($month_start));
             date_default_timezone_set('australia/melbourne');
             $current_date = date("Ymd");
             $calendar_flag = "";
             $date_filter_array = array (
                  array(
                    // 'meta_key' => 'testing',
                    'value' => array($start,$end ),
                    'type' => 'DATE',
                    'compare' => 'between',
                  ),
             );
            echo   $current_month;
            $the_query=new WP_Query(array(
                     'post_type' => 'post',
                     'meta_key' => 'prayer_date',
                     'meta_type'		=> 'DATE',
                      'orderby' => 'prayer_date',
                     'order' 		=> 'ASC',
                     'meta_query' => array(
                         $date_filter_array,
                     ),

                  ));
            if ( $the_query->have_posts() ) :
            	while (   $the_query->have_posts() ) : $the_query->the_post();
              if(get_field('prayer_date') == $current_date){
                $calendar_flag = "is-today";
              }
              if(get_field('prayer_date') > $current_date)
              {
                $calendar_flag = "after-today";
              }
            ?>

            <div class="calendar-item <?php echo $calendar_flag?>" value="<?php echo get_field('prayer_date') ?>">
              <span>
                <?php echo $start = date('d M', strtotime(get_field('prayer_date'))); ?>
              </span>
            </div>
          <?php
              endwhile;
            endif;
           ?>

          </div>
        </div>

      </div>
      <div class="prayer-show">
        <div class="prayer-part part-1 gospel-quo">
          <div class="prayer-date">

          </div>
          <div class="gospel-title">
            <!-- Illness can be a Gift -->
          </div>
          <div class="gospel-content">
            <!-- Where can i go from your spirit? Or where can i flee from your presence? -->
          </div>
          <div class="gospel-quote">
          </div>

          <div class="icon-wrapper">
            <div class="next-icon">
              <img class="svg" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/next-icon.png" alt="">
            </div>
          </div>
        </div>

        <div class="prayer-part part-2 short-des">
          <div class="short-des-lead">

          </div>

          <div class="prayer-date">

          </div>

          <div class="short-des-content">

          </div>

          <div class="icon-wrapper">
            <div class="next-icon">
              <img class="svg" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/next-icon.png" alt="">
            </div>
          </div>
        </div>
        <div class="prayer-part part-3 reflection">
          <div class="reflective-content">
            <div class="prayer-date">
            </div>
            <div class="reflective-que">
            </div>
          </div>
          <div class="timer-area">
            <div class="timer-lead">
              Please set your Reflection Timer
            </div>
            <div class="time-left">
              00:00 min
            </div>
            <div class="time-timer-outer">
              <div class="timer-time">
                <div id="progressBar">
                  <div class="bar-outer">
                    <div class="bar-inner">

                    </div>
                  </div>
                  <div class="circle">

                  </div>
                </div>
              </div>
            </div>

            <div class="timer-start">
              Save & Begin
            </div>
          </div>
          <div class="begin-reflection">
            Begin Reflection
          </div>

        </div>
        <div class="prayer-part part-4 closing-quote">
          <div class="prayer-date">
          </div>
          <div class="closing-content">
          </div>
          <div class="closing-source">
          </div>
        </div>
      </div>

    </div>

    <div class="prev-btn">
      <img class="svg" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/prev.svg" alt="">
    </div>
    <div class="next-btn">
      <img class="svg" src="<?php echo get_template_directory_uri() ?>/front-end/assets/img/next.svg" alt="">
    </div>
  </div>
  <div class="editor-content">
    <!-- <?php the_ID(); ?> -->
  </div>
</div>
<div class="loading-spinner">
  <div class="wrapper">
    <div class="loader">
      <div class="wave top-wave">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
      <div class="wave bottom-wave">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  </div>
</div>

  <div class="no-prayer-today">
    Sorry, No Prayer Today!
  </div>
