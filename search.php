<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package fraynework
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="main-wrapper searchpage editor-content">
			<?php get_template_part( 'template-parts/module', 'top-banner' );	?>
			<article>
				<?php
				global $wp_query;
				if ( have_posts() ) : ?>
				<header class="entry-header search">
					<h1 class="entry-title">Search Results</h1>
				</header><!-- .entry-header -->
				<div class="entry-content results-container">
					<?php get_search_form();?>
					<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'search' );
						endwhile;
					?>
				</div>
				<div class="paging">
					<?php wp_pagenavi(array( 'query' => $wp_query )); ?>
				</div>
					<?php else : ?>
					<header class="entry-header nothing">
						<h1 class="entry-title">Nothing Found</h1>
					</header><!-- .entry-header -->
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<?php endif; ?>
			</article>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
