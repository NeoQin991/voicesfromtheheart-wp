## Fraynework Multimedia Bootstrap Wordpress theme
### Development
Go to the **/front-end** directory

- In terminal type:
 ```npm install```

- For development, type:
 ```npm start``` or ```npm run dev```

- For production, type:
```npm run build```


### Required plugins
- https://www.advancedcustomfields.com/pro/
- https://wordpress.org/plugins/wp-pagenavi/

### Suggested plugins
- https://adminmenueditor.com/
- https://wordpress.org/plugins/intuitive-custom-post-order/
- https://wordpress.org/plugins/mce-table-buttons/

### Remember to
- Replace /front-end/asset/img/logo.svg
- Change the theme name in style.css
