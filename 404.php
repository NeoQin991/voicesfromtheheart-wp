<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package fraynework
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="main-wrapper error-404 not-found editor-content">
				<?php  get_template_part( 'template-parts/module', 'top-banner' );	?>
					<article>
						<header class="entry-header">
							<h1 class="entry-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'fraynework' ); ?></h1>
						</header><!-- .entry-header -->
						<div class="entry-content">
							<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					</article>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
